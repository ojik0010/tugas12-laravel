<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('page.register');
    }
    public function welcome(Request $request)
    {
        // dd($request->all());
        $depan = $request['namadpn'];
        $belakang = $request['namablkg'];

        return view('page.welcome', compact('depan', 'belakang'));
    }
}
